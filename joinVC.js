const client = require('./bot.js')
module.exports = {
  join: function (receievedMessage) {
    // Only try to join the sender's voice channel if they are in one themselves
    if (receievedMessage.guild.me.voiceChannel) {
      receivedMessage.reply('Already connected')
    } else if (receievedMessage.member.voiceChannel) {
      receievedMessage.member.voiceChannel.join()
        .then(connection => { // Connection is an instance of VoiceConnection
          receievedMessage.reply('I have successfully connected to the channel!');
        })
        .catch(console.log);
    } else {
      receievedMessage.reply('You need to join a voice channel first!');
    }
  },
  disconnect: function (receievedMessage) {
    // Leave the voice channel if already connected
    // See if connected to voice voiceChannel
    if (receievedMessage.member.voiceChannel) {
      receievedMessage.member.voiceChannel.leave()
    }
  },
}
