const Discord = require('discord.js');
const client = new Discord.Client();
const auth = require('./auth.json');
const cmd = require('./commands.js');
const joinvc = require('./joinVC.js');
const play = require('./play.js');

client.on('message', (receivedMessage) => {
    if (receivedMessage.author == client.user) { // Prevent bot from responding to its own messages
        return
    }

    if (receivedMessage.content.startsWith("~")) {
        processCommand(receivedMessage)
    }
})

function processCommand(receivedMessage) {
    let fullCommand = receivedMessage.content.substr(1) // Remove the leading exclamation mark
    let splitCommand = fullCommand.split(" ") // Split the message up in to pieces for each space
    let primaryCommand = splitCommand[0] // The first word directly after the exclamation is the command
    let arguments = splitCommand.slice(1) // All other words are arguments/parameters/options for the command

    console.log("Command received: " + primaryCommand)
    console.log("Arguments: " + arguments) // There may not be any arguments

    if (primaryCommand == "help") {
        cmd.helpCommand(arguments, receivedMessage)
    } else if (primaryCommand == "multiply") {
        cmd.multiplyCommand(arguments, receivedMessage)
    } else if (primaryCommand == "ping") {
        cmd.pingCommand(receivedMessage)
    } else if (primaryCommand == "kevin" || primaryCommand == "kebin") {
        cmd.kevinCommand(receivedMessage)
    } else if (primaryCommand == "bo") {
        cmd.boCommand(receivedMessage)
    } else if (primaryCommand == "caleb") {
        cmd.calebCommand(receivedMessage)
    } else if (primaryCommand == "aaron") {
        cmd.aaronCommand(receivedMessage)
    } else if (primaryCommand == "apex") {
       cmd.apexCommand(receivedMessage)
    } else if (primaryCommand == "classic") {
       cmd.classicCommand(receivedMessage)
    } else if (primaryCommand == "one") {
       cmd.oneCommand(receivedMessage)
    } else if (primaryCommand == "timeout") {
       cmd.timeoutCommand(fullCommand, receivedMessage, arguments)
    } else if (primaryCommand == "join") {
      joinvc.join(receivedMessage)
    } else if (primaryCommand == "disconnect") {
      joinvc.disconnect(receivedMessage)
    //} else if (primaryCommand == "play") {
    //  play.run(receivedMessage, arguments)
    } else {
        receivedMessage.channel.send("I don't understand the command. Try `!help` or `!multiply`")
    }
}

client.on('voiceStateUpdate', (oldMember, newMember) => {
    let newUserChannel = newMember.voiceChannel
    let oldUserChannel = oldMember.voiceChannel

    if (oldUserChannel === undefined && newUserChannel !== undefined) {
        // User joins voice channel
        if (newMember.id === '135965518438989833') {
          newMember.addRole("552731970837282826");
          setTimeout(() => {newMember.removeRole('552731970837282826');}, 30 * 1000);
        }
    }
})


client.login(auth.token)