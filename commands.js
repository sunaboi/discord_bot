module.exports = {
  helpCommand: function (arguments, receivedMessage) {
      if (arguments.length > 0) {
          receivedMessage.channel.send("It looks like you might need help with " + arguments)
      } else {
          receivedMessage.channel.send("I'm not sure what you need help with. Try `!help [topic]`")
      }
  },

  multiplyCommand: function (arguments, receivedMessage) {
      console.log(arguments);
      if (arguments.length < 2) {
          receivedMessage.channel.send("Not enough values to multiply. Try `!multiply 2 4 10` or `!multiply 5.2 7`")
          return
      }
      let product = 1
      arguments.forEach((value) => {
          product = product * parseFloat(value)
      })
      receivedMessage.channel.send("The product of " + arguments + " multiplied together is: " + product.toString())
  },

  pingCommand: function (receivedMessage) {
    receivedMessage.channel.send('Pong!')
  },

  kevinCommand: function (receivedMessage) {
    receivedMessage.channel.send('is a gay chinese crackhead.')
  },

  boCommand: function (receivedMessage) {
    receivedMessage.channel.send('is toxic')
  },

  calebCommand: function (receivedMessage) {
    receivedMessage.channel.send('large think')
  },

  aaronCommand: function (receivedMessage) {
    recievedMessage.channel.send('dude stop')
  },

  apexCommand: function (receivedMessage) {
     let apexRole = receivedMessage.guild.roles.find(role => role.name === "Apex")
     receivedMessage.channel.send(apexRole + " eskiti")
 },

 classicCommand: function (receivedMessage) {
    receivedMessage.channel.send("<@122583885568671744>")
 },

 oneCommand: function (receievedMessage) {
     let oneRole = receievedMessage.guild.roles.find(role => role.name === "ONE")
     receievedMessage.member.addRole(oneRole)
 },

  timeoutCommand: function (fullCommand, recievedMessage, arguments) {
    if (recievedMessage.author.id == "132418402408071168" || recievedMessage.author.id == "132558880537313282") {
      // Find guild member ID
      let mutedMember = recievedMessage.mentions.members.first()
      // set timeout length
      let timeoutLength = 10*1000
      if (arguments.length == 2) {
        timeoutLength = arguments[1]*1000
        recievedMessage.channel.send(mutedMember + " muted for " + arguments[1] + " seconds")
      } else {
        recievedMessage.channel.send("Too many or little arguments, I'll just timeout " + mutedMember + " for 10 seconds")
      }
      // Set timeout - move member to afk channel
      mutedMember.setMute(true)
      setTimeout(() => {mutedMember.setMute(false);}, timeoutLength);
    } else {
      recievedMessage.channel.send("You cannot use this command")
    }
  }
};
