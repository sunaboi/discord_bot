const ytdl = require('ytdl-core');

exports.run = async (receivedMessage, arguments) => {
  // Check if auther is connected to a voice channel
  if (!receivedMessage.member.voiceChannel) {
    recievedMessage.reply("Connect to a voice channel");
  }
  // Check if bot is already connected to a voice channel
  if (receievedMessage.guild.me.voiceChannel) {
    receievedMessage.reply("Already connected to a channel");
  }
  // Check if author input a url
  if (!arguments[0]) {
    receivedMessage.reply("Enter a url");
  }
  // Validate info
  let validate = await ytdl.validateURL(arguments[0]);
  // Check Validation
  if (!validate) {
    receievedMessage.reply('Enter a valid url');
  }
  // Validate will contain boolean if the url is valid or not

  // Fetcg video info
  let info = await ytdl.getInfo(arguments[0]);
  // Store authors guild channel
  let connection = await message.member.voiceChannel.join();
  // Play song
  let dispatcher = await connection.play(ytdl(arguments[0], {filter: 'audioonly'}));
  // Now playing
  receivedMessage.reply(`Now playing ${info.title}`);
}
